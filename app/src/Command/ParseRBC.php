<?php

namespace App\Command;

use App\Service\Parser\ParserInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseRBC extends Command
{
    /**
     * @var string
     */
    public const NAME = 'parse:rbc';

    /**
     * @var string
     */
    protected static $defaultName = self::NAME;

    /**
     * @var ParserInterface
     */
    private $parser;

    public function __construct(
        ParserInterface $parser
    ){
        parent::__construct();
        $this->parser = $parser;
    }

    protected function configure()
    {
        $this
            ->setDescription('Parse rbc site news')
            ->addArgument('number', InputArgument::OPTIONAL, 'Number of news (default value 15)')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $numberOfNews = $input->getArgument('number') ?? 15;
        $output->writeln('Parsing first <comment>' . $numberOfNews . '</comment> news');
        $output->writeln('You can change it by adding argument ex: <comment>php bin/console parse:rbc 5</comment>');

        try {
            $this->parser->parse($numberOfNews);
        } catch (\Exception $e) {
            $output->writeln('Error: <fg=black;bg=red>' . $e->getMessage() . '</>');
        }
    }
}