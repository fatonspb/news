<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends AbstractController
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var NewsRepository
     */
    private $newsRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        NewsRepository $newsRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->newsRepository = $newsRepository;
    }

    /**
     * @Route("/", name="news_list")
     * @return Response
     */
    public function list(): Response
    {
        $news = $this->newsRepository->findAll();

        return $this->render('newsList.html.twig', ['news' => $news]);
    }

    /**
     * @Route("/{categorySlug}", name="category")
     * @param string $categorySlug
     * @return Response
     */
    public function category(string $categorySlug): Response
    {
        $category = $this->categoryRepository->findOneBy(['slug' => $categorySlug]);

        if (!$category) {
            return $this->render('404.html.twig', ['msg' => 'Category not found.']);
        }

        $news = $category
            ? $category->getNews()
            : [];

        return $this->render('newsList.html.twig', ['news' => $news]);
    }

    /**
     * @Route("/{categorySlug}/{newsSlug}", name="news_item")
     *
     * @param string $categorySlug
     * @param string $newsSlug
     * @return Response
     */
    public function show(string $categorySlug, string $newsSlug): Response
    {
        $news = $this->newsRepository->findOneBy(['slug' => $newsSlug]);

        if (!$news) {
            return $this->render('404.html.twig', ['msg' => 'News not found.']);
        }

        return $this->render('newsItem.html.twig', ['news' => $news]);
    }
}