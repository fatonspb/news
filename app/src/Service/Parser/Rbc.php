<?php

namespace App\Service\Parser;

use App\Service\HttpClient;
use App\Service\PageNotFoundExeption;
use Exception;
use App\Service\NewsService;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\DomCrawler\Crawler;
use Psr\Log\LoggerInterface;

class Rbc implements ParserInterface
{
    use LoggerAwareTrait;

    /**
     * @var NewsService
     */
    private $newsService;
    /**
     * @var HttpClient
     */
    private $httpClient;
    /**
     * @var string
     */
    private $url;
    /**
     * @var string
     */
    private $uidRegExp;
    /**
     * @var int
     */
    private $descriptionLimit;
    /**
     * @var string
     */
    private $newsListItemSelector;
    /**
     * @var string
     */
    private $newsTitleSelector;
    /**
     * @var string
     */
    private $newsDescriptionSelector;
    /**
     * @var string
     */
    private $newsListItemInfoSelector;
    /**
     * @var string
     */
    private $newsImageSelector;

    public function __construct(
        LoggerInterface $logger,
        NewsService $newsService,
        HttpClient $httpClient,
        string $url,
        string $uidRegExp,
        int $descriptionLimit,
        string $newsListItemSelector,
        string $newsTitleSelector,
        string $newsDescriptionSelector,
        string $newsListItemInfoSelector,
        string $newsImageSelector
    ) {
        $this->logger = $logger;
        $this->newsService = $newsService;
        $this->httpClient = $httpClient;
        $this->url = $url;
        $this->uidRegExp = $uidRegExp;
        $this->descriptionLimit = $descriptionLimit;
        $this->newsListItemSelector = $newsListItemSelector;
        $this->newsListItemInfoSelector = $newsListItemInfoSelector;
        $this->newsTitleSelector = $newsTitleSelector;
        $this->newsDescriptionSelector = $newsDescriptionSelector;
        $this->newsImageSelector = $newsImageSelector;
    }

    /**
     * @param int $numberOfNews
     * @throws PageNotFoundExeption
     */
    public function parse(int $numberOfNews): void
    {
        $this->logger->info('Parsing rbc');

        $url = $this->url;
        $html = $this->httpClient->getHtml($url);
        $crawler = new Crawler($html);

        // get news list
        $crawler->filter($this->newsListItemSelector)->slice(0, $numberOfNews)->each(function (Crawler $node) {
            $this->parseNewsLink($node);
        });
    }

    /**
     * Check if we have this news.
     * Parse if we don`t have it.
     *
     * @param Crawler $node
     */
    private function parseNewsLink(Crawler $node): void
    {
        $newsLink = $node->attr('href');

        preg_match_all($this->uidRegExp, $newsLink, $matches);
        $uid = $matches[1][0] ?? null;

        if ($uid) {
            $newsExist = $this->newsService->isNewsExist($uid);

            if (!$newsExist) {
                $info = $node->filter($this->newsListItemInfoSelector)->text();
                $categoryTitle = explode(',', $info)[0] ?? null;

                if ($categoryTitle) {
                    try {
                        $this->parseFullNews($newsLink, $uid, $categoryTitle);
                    } catch (PageNotFoundExeption $e) {
                        $this->logger->error($e->getMessage());
                    }
                }
            }
        }
    }

    /**
     * @param string $link
     * @param string $uid
     * @param string $categoryTitle
     * @throws PageNotFoundExeption
     */
    private function parseFullNews(string $link, string $uid, string $categoryTitle): void
    {
        $html = $this->httpClient->getHtml($link);
        $crawler = new Crawler($html);

        try {
            // parse title
            $title = $crawler->filter($this->newsTitleSelector)->attr('content');

            // parse img
            $imageNode = $crawler->filter($this->newsImageSelector);
            $imageUrl = $imageNode->count()
                ? $imageNode->first()->attr('src')
                : null;

            // parse body
            $body = [];
            $crawler->filter($this->newsDescriptionSelector)->first()->filter('span,p')->each(function (Crawler $crawler) use (&$body) {
                $body[] = $this->wrapParagraph($crawler->html());
            });
            $body = implode("\n", array_filter($body));

            // prepare description
            $description = mb_substr($body, 0, $this->descriptionLimit);

            // save
            $this->newsService->saveNews($uid, $categoryTitle, $title, $description, $body, $imageUrl);

        } catch (Exception $e) {
            $this->logger->error(
                'Error parsing news',
                [
                    'url' => $link,
                    'error' => $e->getMessage()
                ]
            );
        }
    }

    /**
     * @param string $html
     * @return string
     */
    private function wrapParagraph(string $html): string
    {
        return '<p>' . trim($html) . '</p>';
    }
}