<?php

namespace App\Service\Parser;

interface ParserInterface
{
    public function parse(int $numberOfNews): void;
}