<?php

namespace App\Service;

use App\Entity\Category;
use App\Entity\News;
use App\Repository\CategoryRepository;
use App\Repository\NewsRepository;
use Doctrine\ORM\EntityManagerInterface;

class NewsService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var NewsRepository
     */
    private $newsRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(
        EntityManagerInterface $em,
        NewsRepository $newsRepository,
        CategoryRepository $categoryRepository
    ) {
        $this->em = $em;
        $this->newsRepository = $newsRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param string $uid
     * @param string $categoryTitle
     * @param string|null $title
     * @param string $description
     * @param string $body
     * @param string|null $imageUrl
     * @throws Exception
     */
    public function saveNews(string $uid, string $categoryTitle, ?string $title, string $description, string $body, ?string $imageUrl): void
    {
        // get / create category
        $category = $this->categoryRepository->findOneBy(['title' => $categoryTitle]);

        if (!$category) {
            $category = new Category($categoryTitle);
            $this->em->persist($category);
        }

        // save news
        $news = new News($uid, $title, $category, $description, $body, $imageUrl);
        $this->em->persist($news);
        $this->em->flush();
    }

    public function isNewsExist($uid)
    {
        return $this->newsRepository->findOneBy(['uid' => $uid]);
    }
}