<?php

namespace App\Service;

use Exception;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Client;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

class HttpClient
{
    use LoggerAwareTrait;
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(
        LoggerInterface $logger
    )
    {
        $this->client = new Client();
        $this->logger = $logger;
    }

    /**
     * @param string $url
     * @return false|string
     * @throws PageNotFoundExeption
     */
    public function getHtml(string $url): string
    {
        $response = $this->client->request('GET', $url);

        if ($response->getStatusCode() === 200) {
            return $response->getBody();
        } else {
            $msg = 'target page is not available';
            $this->logger->error($msg, ['url' => $url]);

            throw new PageNotFoundExeption($msg);
        }
    }
}