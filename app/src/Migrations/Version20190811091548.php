<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190811091548 extends AbstractMigration
{
    /**
     * @var string
     */
    private const NEWS_TABLE_NAME = 'news';

    /**
     * @var string
     */
    private const CATEGORY_TABLE_NAME = 'category';

    public function getDescription() : string
    {
        return 'Startup migration';
    }

    public function up(Schema $schema) : void
    {
        $newsTable = $schema->createTable(self::NEWS_TABLE_NAME);
        $newsTable->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $newsTable->addColumn('category_id', 'integer', ['unsigned' => true]);
        $newsTable->addColumn('uid', 'string');
        $newsTable->addColumn('slug', 'string');
        $newsTable->addColumn('title', 'string');
        $newsTable->addColumn('description', 'string');
        $newsTable->addColumn('body', 'text');
        $newsTable->addColumn('image', 'string', ['notnull' => false]);
        $newsTable->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP']);
        $newsTable->setPrimaryKey(['id']);
        $newsTable->addUniqueIndex(['uid']);
        $newsTable->addUniqueIndex(['slug']);

        $categoryTable = $schema->createTable(self::CATEGORY_TABLE_NAME);
        $categoryTable->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $categoryTable->addColumn('title', 'string');
        $categoryTable->addColumn('slug', 'string');
        $categoryTable->setPrimaryKey(['id']);
        $categoryTable->addUniqueIndex(['title']);
        $categoryTable->addUniqueIndex(['slug']);

        $newsTable->addForeignKeyConstraint($categoryTable, ['category_id'], ['id']);
    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable(self::NEWS_TABLE_NAME);
        $schema->dropTable(self::CATEGORY_TABLE_NAME);
    }
}
