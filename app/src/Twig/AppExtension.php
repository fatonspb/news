<?php

namespace App\Twig;

use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(
        CategoryRepository $categoryRepository,
        RequestStack $requestStack
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->requestStack = $requestStack;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('getCategories', [$this, 'getCategories']),
        ];
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categorySlug = $this->requestStack->getCurrentRequest()->attributes->get('categorySlug') ?? null;

        $result = [];
        $categories = $this->categoryRepository->findAll();

        foreach ($categories as $category) {
            $slug = $category->getSlug();
            $result[] = [
                'slug' => $slug,
                'title' => $category->getTitle(),
                'isActive' => $slug === $categorySlug,
            ];
        }

        return $result;
    }
}