## Installing

pull code, build container, install vendors and run migrations

```
git clone https://bitbucket.org/fatonspb/news.git
cd news
docker-compose up -d
docker exec -it news_php_1 composer install --prefer-dist --no-dev --no-scripts --no-progress --no-suggest; composer clear-cache
docker exec -it news_php_1 php bin/console doctrine:migrations:migrate --no-interaction --allow-no-migration
```

## Using
run console task to parse data
```
docker exec -it news_php_1 php bin/console parse:rbc
```

Open http://localhost:88

## Additional
Use port `3307` if you want to connect to MySql
